console.log("Hey, what are you checking on the console?");

document.addEventListener('DOMContentLoaded', function() {
  const downloadBtn = document.getElementById('downloadBtn');

  downloadBtn.addEventListener('click', function() {
    const cvUrl = './resume6.pdf'; // Replace with the actual URL of your CV

    const newTab = window.open(cvUrl, '_blank');
    newTab.focus();
  });
});



