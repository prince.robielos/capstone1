# My Portfolio Website

It's static website with responsive design. I build it, using `HTML, CSS and Bootstrap` and few line of codes for `JavaScript` to make it interactive. Then I add few line of codes for `Google Analytics` for monitoring and traffics. Thanks to GitHub repository I was able to deploy my first static and responsive web profile even it is a sub-domain.

- Visit my [Web Portfolio](https://rprinceroger.github.io/mywebportfolio/)!
